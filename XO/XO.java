import java.util.Scanner;

public class XO {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String[][] space = new String[3][3];
        String[] turn = {"X","O","X","O","X","O","X","O","X"};
        int row ;
        int column;
        int win = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                space[i][j] = "-";
            }
        } 
        for (int a = 0; a < 3; a++){
            for (int b = 0; b<3; b++){
                System.out.print(space[a][b] + " ");
            }
        System.out.println();
        }

        for(int r = 0; r < 9; r++){
            while (true) {
                System.out.println("TURN "+turn[r]);
                System.out.print("Enter row number(1-3):");
                row = input.nextInt();
                System.out.print("Enter column number(1-3):");
                column = input.nextInt();
                row-=1;
                column-=1;
                System.out.println();
                if (row >= 0 && row <3 && column >= 0 && column <3 && space[row][column] != "X" && space[row][column] != "O" ) {
                    break;
                }else;{
                    System.out.println("Please enter the correct number and ensure it does not duplicate an existing position..");
                    for (int a = 0; a < 3; a++){
                        for (int b = 0; b<3; b++){
                            System.out.print(space[a][b] + " ");
                        }
                    System.out.println();
                    }
                }
            }
            space[row][column]=turn[r];
            for (int i = 0; i < 3; i++){
                for (int j = 0; j<3; j++){
                    System.out.print(space[i][j] + " ");
                }
            System.out.println();
            }
            for(int i = 0;i < 3; i++){
                if (space[i][0]=="X" && space[i][1]=="X" && space[i][2]=="X" || 
                    space[i][0]=="O" && space[i][1]=="O" && space[i][2]=="O") {
                    win=1;
                    break;
                }
            }
            for(int i = 0;i < 3; i++){
                if (space[0][i]=="X" && space[1][i]=="X" && space[2][i]=="X" || 
                    space[0][i]=="O" && space[1][i]=="O" && space[2][i]=="O") {
                    win=1;
                    break;
                }
            }
            if (space[0][0]=="X" && space[1][1]=="X" && space[2][2]=="X" || 
                space[0][0]=="O" && space[1][1]=="O" && space[2][2]=="O" ||
                space[0][2]=="X" && space[1][1]=="X" && space[2][0]=="X" || 
                space[0][2]=="O" && space[1][1]=="O" && space[2][0]=="O" ) {
                win=1;
            }

            if (win == 1){
                System.out.println("PLAYER "+turn[r]+" !!!win!!!");
                break;
            } 
        }
        if (win == 0) {
            System.out.println("!!!TIE!!!");
        }
    }
}
